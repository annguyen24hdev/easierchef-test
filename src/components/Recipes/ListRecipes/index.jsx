import React, { useState } from "react";
import "./index.scss";
import { makeStyles } from "@material-ui/core/styles";

import { red } from "@material-ui/core/colors";

import FavoriteIcon from "@material-ui/icons/Favorite";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import {
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
  Avatar,
  IconButton,
  Typography,
  Grid,
  Menu,
  MenuItem,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: red[500],
  },
}));

const ListRecipes = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [recipeId, setRecipeId] = useState(null);
  const classes = useStyles();

  const handleDelete = async () => {
    if (recipeId) {
      await props.onDelete(recipeId);
      setAnchorEl(null);
      setRecipeId(null);
    }
  };

  const handleClick = (event, item) => {
    console.log("item: ", item);
    setRecipeId(item.id);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="listRecipces">
      <Grid container spacing={4}>
        {props.data &&
          props.data.length > 0 &&
          props.data.map((item, i) => (
            <Grid key={i} item xs={12} sm={6} md={4}>
              <Card className={classes.root}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                      {item.name.charAt(0).toUpperCase()}
                    </Avatar>
                  }
                  action={
                    <>
                      <IconButton
                        aria-label="settings"
                        onClick={(e) => handleClick(e, item)}
                      >
                        <MoreVertIcon />
                      </IconButton>
                      <Menu
                        id="simple-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                      >
                        <MenuItem onClick={handleDelete}>Delete</MenuItem>
                      </Menu>
                    </>
                  }
                  title={item.name}
                  subheader={`${item.time} minutes`}
                />
                <CardMedia
                  className={classes.media}
                  image={item.image}
                  title=""
                />
                <CardContent>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    <strong>Cooking difficulty: </strong> {item.difficulty}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    <strong>Calories: </strong> {item.noCalo ? "No" : "Yes"}
                  </Typography>
                </CardContent>
                <CardActions disableSpacing>
                  <IconButton
                    color={item.favorite ? "secondary" : "default"}
                    aria-label="add to favorites"
                    onClick={() => props.onFavorite(item.id)}
                  >
                    <FavoriteIcon />
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default ListRecipes;
