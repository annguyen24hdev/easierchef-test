import React, { useState, useEffect } from "react";
import "./index.scss";

// ui
import { Button, Dialog, DialogContent, DialogTitle } from "@material-ui/core";
// components
import NewRecipe from "./NewRecipe";
import ListRecipes from "./ListRecipes";

const RecipesContent = (props) => {
  const [open, setOpen] = useState(false);
  const [favorites, setFavorites] = useState([]);
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    const store = JSON.parse(localStorage.getItem("recipes")) || [];
    onSetData(store);
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteRecipe = async (id) => {
    const newRecipes = await recipes.filter((el) => el.id !== id);
    onSetData(newRecipes);
  };

  const handleFavorite = async (id) => {
    const newData = [];

    for (let i = 0; i < recipes.length; i++) {
      const el = recipes[i];

      if (el.id === id) {
        el.favorite = el.favorite ? !el.favorite : true;
        console.log("el.favorite: ", el.favorite);
        if (el.favorite) {
          newData.unshift(el);
        } else {
          newData.push(el);
        }
      } else {
        newData.push(el);
      }
    }

    onSetData(newData);
  };

  const onSetData = async (data) => {
    const newSort = data.sort((x, y) => y.favorite - x.favorite);
    await setRecipes(newSort);
    await localStorage.setItem("recipes", JSON.stringify(newSort));

    const _favorites = newSort.filter((el) => el.favorite === true);
    setFavorites(_favorites);
  };

  return (
    <div>
      <h3>Recipes</h3>
      <Button variant="contained" color="primary" onClick={handleClickOpen}>
        Add Recipe
      </Button>

      <ListRecipes
        data={recipes}
        onDelete={handleDeleteRecipe}
        onFavorite={handleFavorite}
      />

      <h3>Favorite Brands</h3>
      <ListRecipes
        data={favorites}
        onDelete={handleDeleteRecipe}
        onFavorite={handleFavorite}
      />

      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">New Recipe</DialogTitle>
        <DialogContent>
          <NewRecipe onClose={handleClose} onSetData={setRecipes} />
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default RecipesContent;
