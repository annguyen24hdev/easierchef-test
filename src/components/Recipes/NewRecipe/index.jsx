import React, { useState } from "react";
import "./index.scss";
import { v4 as uuidv4 } from "uuid";

import {
  TextField,
  Radio,
  FormControlLabel,
  FormLabel,
  FormControl,
  Checkbox,
  Button,
  RadioGroup,
  Grid,
} from "@material-ui/core";

const NewRecipe = (props) => {
  const [values, setValues] = useState({
    name: "",
    image: "",
    time: "",
    noCalo: false,
    difficulty: "easy",
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    const recipe = {
      ...values,
      id: uuidv4(),
    };

    const recipes = JSON.parse(localStorage.getItem("recipes")) || [];
    recipes.push(recipe);

    localStorage.setItem("recipes", JSON.stringify(recipes));
    props.onSetData(recipes);
    props.onClose();
  };

  const handleChangeInput = (event) => {
    const { name, value } = event.target;
    setValues({ ...values, [name]: value });
  };

  const handleChangeDifficulty = (event) => {
    setValues({ ...values, [event.target.name]: event.target.value });
  };

  const handleChangeCheckbox = (event) => {
    setValues({ ...values, [event.target.name]: event.target.checked });
  };

  return (
    <div className="newRecipe">
      <form onSubmit={handleSubmit}>
        <TextField
          className="textField"
          label="Name"
          name="name"
          type="text"
          onChange={handleChangeInput}
          fullWidth
        />

        <TextField
          className="textField"
          label="Image URL"
          name="image"
          onChange={handleChangeInput}
          fullWidth
        />

        <TextField
          className="textField"
          label="Time (mins)"
          name="time"
          onChange={handleChangeInput}
          type="number"
          fullWidth
        />

        <Grid container spacing={4}>
          <Grid item sm={6}>
            <FormControl component="fieldset" fullWidth>
              <FormLabel>Cooking difficulty</FormLabel>
              <RadioGroup
                aria-label="difficulty"
                name="difficulty"
                value={values.difficulty}
                onChange={handleChangeDifficulty}
                fullWidth
              >
                <FormControlLabel
                  value="easy"
                  control={<Radio />}
                  label="Easy"
                />
                <FormControlLabel
                  value="medium"
                  control={<Radio />}
                  label="Medium"
                />
                <FormControlLabel
                  value="hard"
                  control={<Radio />}
                  label="Hard"
                />
              </RadioGroup>
            </FormControl>
          </Grid>

          <Grid item sm={6}>
            <FormControlLabel
              fullWidth
              control={
                <Checkbox
                  checked={values.noCalo}
                  onChange={handleChangeCheckbox}
                  name="noCalo"
                  color="primary"
                />
              }
              label="No of calories"
            />
          </Grid>
        </Grid>

        <div className="actions">
          <Button onClick={props.onClose} variant="contained" color="default">
            Cancel
          </Button>
          <Button onClick={handleSubmit} variant="contained" color="primary">
            Submit
          </Button>
        </div>
      </form>
    </div>
  );
};

export default NewRecipe;
