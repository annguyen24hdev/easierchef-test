import React from "react";
import "./index.scss";

import RecipesContent from "../Recipes";

const DashboardContent = (props) => {
  return (
    <div className="dashboardContent">
      <RecipesContent />
    </div>
  );
};

export default DashboardContent;
